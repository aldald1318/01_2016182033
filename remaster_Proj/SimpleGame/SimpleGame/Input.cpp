#include "stdafx.h"
#include "Input.h"

#include "Command.h"

namespace Input
{
	std::unordered_multimap<int, Command*>	_allCommands;
	bool KeyFlag[256];
}

void Input::ClearCommand()
{
	for (auto& cmd : Input::_allCommands)
	{
		SAFE_DELETE_PTR(cmd.second);
	}
	Input::_allCommands.clear();
}

bool Input::IsKeyDown(int key)
{
	if (GetAsyncKeyState(key) & 0x8000) {
		if (!KeyFlag[key])
			return KeyFlag[key] = true;
	}
	return false;
}

bool Input::IsKeyUp(int key)
{
	if (GetAsyncKeyState(key) & 0x8000)
		KeyFlag[key] = true;
	else
		if (KeyFlag[key])
			return !(KeyFlag[key] = false);
	return false;
}
