#pragma once
#include "Pawn.h" /*헤더중복 우려*/

class Command abstract
{
public:
	explicit Command() = default;
	virtual ~Command() = default;
	virtual void execute(Pawn* pawn) = 0;

public:
	void ChangePawnState() {}
};

class ForceCommand : public Command
{
public:
	explicit ForceCommand(float fx, float fy, float fz) :
		m_ApplyForce(fx,fy,fz) {}
	virtual ~ForceCommand() {}

public:
	virtual void execute(Pawn* pawn);

private:
	DirectX::XMFLOAT3 m_ApplyForce;
};

class SceneCommand : public Command
{

};

class StateCommand : public Command
{
public:
	virtual void execute(Pawn* pawn);
};