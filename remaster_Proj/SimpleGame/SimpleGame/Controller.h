#pragma once
#include "IComponent.h"

class Command;
class Controller : public IComponent
{
protected:
	enum StateType
	{
		Move,
		Jump,
		Shoot,
	};

public:
	explicit Controller() = default;
	virtual ~Controller() = default;

public:
	virtual void update(const float DeltaT) = 0;
	virtual void handleInput() = 0;

protected:
	virtual void PressedOrder(int vk, StateType type) = 0;
	virtual void ReleaseOrder(int vk, StateType type) = 0;
	virtual void OrderControl(int vk, StateType type) = 0;
	
public:
	std::vector<Command*>	_commands;
};

class Pawn;
class PlayerController : public Controller
{
public:
	explicit PlayerController(Pawn* owner);
	virtual ~PlayerController();
	
public:
	virtual void update(const float DeltaT);
	virtual void handleInput() override;

private:
	virtual void PressedOrder(int vk, StateType type);
	virtual void ReleaseOrder(int vk, StateType type);
	virtual void OrderControl(int vk, StateType type);

private:
	Pawn* m_Owner;
};