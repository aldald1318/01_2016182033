#pragma once
#include "Pawn.h"

/* Character 생성 양식
	IGameObject::CreateObjectID<Character>();
	m_Physics->SetPossedActorID(_objectID);
	m_Graphics->SetPossedActorID(_objectID);
	m_CharacterController->SetPossedActorID(_objectID);
*/

/* Character
@ 플레이어, 클라이언트 유저 자신
@ Pawn을 상속받고 앱에서 단 하나만 존재한다.
*/
class Character : public Pawn
{
public:
	explicit Character();
	virtual ~Character();

public:
	virtual void Initialize() override;
	virtual void Release() override;

public:
	virtual void Update(const float DeltaT);

};

