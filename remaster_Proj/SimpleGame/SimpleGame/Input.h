#pragma once

#define isPressed(vk) GetAsyncKeyState(vk) & 0x8000


class Command;
namespace Input
{
	extern std::unordered_multimap<int, Command*> _allCommands;
	extern bool KeyFlag[256];
}

namespace Input
{
	/* CreateCommand
	@ Input Virtual Key & Command
	*/
	template<class TCommand, class... Args>
	void CreateCommand(int virtualKey, Args&&... Ax)
	{
		if (virtualKey != -1)
		{
			Command* command = new TCommand(std::forward<Args>(Ax)...);
			Input::_allCommands.emplace(virtualKey, command);
		}
	}

	void ClearCommand();


	bool IsKeyDown(int key);
	bool IsKeyUp(int key);
}