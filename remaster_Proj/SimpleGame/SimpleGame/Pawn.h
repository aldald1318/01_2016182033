#pragma once
#include "Actor.h"

/* Pawn
@ 조작이 가능한 오브젝트
@ Actor를 상속받음
@ UI Command를 소유함
*/

class State;
class PlayerController;
class Pawn : public Actor
{
public:
	explicit Pawn();
	virtual ~Pawn();

public:
	virtual void Initialize() override;
	virtual void Release() override;

public:
	virtual void Update(const float DeltaT);

public:
	template <class TState>
	State* ChangeState(bool PopCurrentState, int vk)
	{
		if (PopCurrentState) PopState();
		State* curState = new TState;
		curState->SetVK(vk);
		curState->Enter(this);
		m_States.push(curState);
		return curState;
	}

	bool PopState();

public:
	void SetController();
	PlayerController* GetController();

public:
	// State
	std::stack<State*> m_States;
	// Controller
	PlayerController* m_Controller;

	int dieLayer = -1;

	bool isHulk = false;
	int hulklife = 2;


	bool isCharacter = false;
};

