#pragma once
#include "Sound.h"

extern Sound* g_sound;

namespace Graphics
{
	extern const char* g_Appname;
	extern const size_t g_ClientWidth;
	extern const size_t g_ClientHeight;
}

// XMMath Fucntion
using namespace DirectX;

inline XMVECTOR XM_CALLCONV GreaterOrEqual(FXMVECTOR v1, FXMVECTOR v2)
{
	return XMVectorGreaterOrEqual(v1, v2);
}

inline bool XM_CALLCONV AllTrue(FXMVECTOR v)
{
	uint32_t out;
	XMVectorEqualIntR(&out, v, XMVectorTrueInt());
	return XMComparisonAllTrue(out);
}

inline XMVECTOR XM_CALLCONV Evaluate(FXMVECTOR v1)
{
	if (AllTrue(v1))
		return XMVectorSplatOne();
	else
		return XMVectorZero();

}