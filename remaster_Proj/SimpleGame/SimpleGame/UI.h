#pragma once
class UI
{
public:
	UI();
	~UI();

public:
	void Render();
	void SetTexture(std::string id);

private:
	int m_TextureID;
	XMFLOAT3 m_Position;
};

