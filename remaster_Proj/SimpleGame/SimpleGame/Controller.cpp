#include "stdafx.h"
#include "Controller.h"
#include "Input.h"

#include "State.h"
#include "Command.h"
#include "Pawn.h"

PlayerController::PlayerController(Pawn* owner)
{
	m_Owner = owner;
}

PlayerController::~PlayerController()
{
}

void PlayerController::update(const float DeltaT)
{
	handleInput();

	for (auto& cmd : _commands) {
		cmd->execute(m_Owner);
		_commands.pop_back();
	}
}

void PlayerController::handleInput()
{
	// Move
	OrderControl(VK_UP, StateType::Move);
	OrderControl(VK_DOWN, StateType::Move);
	OrderControl(VK_LEFT, StateType::Move);
	OrderControl(VK_RIGHT, StateType::Move);

	// Jump
	OrderControl(VK_SPACE, StateType::Jump);

	// Shoot
	OrderControl(VK_CONTROL, StateType::Shoot);
}

void PlayerController::OrderControl(int vk, StateType type)
{
	if (isPressed(vk))
	{
		PressedOrder(vk, type);
	}
	if (Input::IsKeyUp(vk))
	{
		ReleaseOrder(vk,type);
	}
}

void PlayerController::PressedOrder(int vk, StateType type)
{
	switch (type)
	{
	case StateType::Move:
		m_Owner->GetPhysics().SetDirection(vk);
		m_Owner->ChangeState<MoveState>(true, vk);
		break;
	case StateType::Jump:
		// Jump Motion
		m_Owner->ChangeState<JumpState>(true, vk);
		break;
	case StateType::Shoot:
		m_Owner->ChangeState<ShootState>(true, vk);
		break;

	default:
		break;
	}

	_commands.emplace_back(Input::_allCommands.find(vk)->second);
}

void PlayerController::ReleaseOrder(int vk, StateType type)
{
	switch (type)
	{
	case StateType::Move:
		m_Owner->ChangeState<IdleState>(true, vk);
		break;
	case StateType::Jump:
		m_Owner->PopState();
		break;
	case StateType::Shoot:
		m_Owner->PopState();
		break;
	default:
		break;
	}
}
