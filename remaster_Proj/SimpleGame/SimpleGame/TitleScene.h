#pragma once
#include "Scene.h"

class Dumb;
class TitleScene : public Scene
{
public:
	explicit TitleScene();
	virtual ~TitleScene();

public:
	virtual bool Enter() override;
	virtual void Exit() override;
	virtual void Update(const float& fDeltaTime) override;

public:
	void LoadResources();
	void LoadSounds();

public:
	Dumb* title;
};

