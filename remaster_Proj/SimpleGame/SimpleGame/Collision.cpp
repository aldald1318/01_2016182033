#include "stdafx.h"
#include "Collision.h"
#include "Actor.h"
#include "Bullet.h"

using namespace DirectX;

bool Collision::IsOverlap(Actor* a1, Actor* a2, int type)
{
	switch (type)
	{
	case 0:
		//BBOverlapTEst
		if (a1 != nullptr && a2 != nullptr) {
			if (a1->GetObjectType() == a2->GetObjectType())
				return false;
			return BBOverlapTest(a1, a2);
		}
		break;
	case 1:
		break;
	}
	return false;
}

#include "Character.h"
bool Collision::IsOverlap(Bullet* a1, Actor* a2, int type)
{
	switch (type)
	{
	case 0:
		//BBOverlapTEst
		if (a2->GetObjectType() == typeid(Character).hash_code())
			return false;
		return BBOverlapTest(a1, a2);
		break;
	case 1:


		break;
	}
	return false;

	return false;
}

void Collision::ProcessCollision(Actor* pFirst, Actor* pSecond)
{
	float aMass = pFirst->GetPhysics().m_Mass;
	XMFLOAT3 aForce = pFirst->GetPhysics().m_Velocity;
	float bMass = pSecond->GetPhysics().m_Mass;
	XMFLOAT3 bForce = pSecond->GetPhysics().m_Velocity;

	float afVX = 0, afVY = 0, afVZ = 0;
	float bfVX = 0, bfVY = 0, bfVZ = 0;

	afVX = ((aMass - bMass) / (aMass + bMass)) * aForce.x
		+ ((2.f * bMass) / (aMass + bMass)) * bForce.x;
	afVY = ((aMass - bMass) / (aMass + bMass)) * aForce.y
		+ ((2.f * bMass) / (aMass + bMass)) * bForce.y;
	afVZ = ((aMass - bMass) / (aMass + bMass)) * aForce.z
		+ ((2.f * bMass) / (aMass + bMass)) * bForce.z;

	bfVX = ((2.f * aMass) / (aMass + bMass)) * aForce.x
		+ ((bMass - aMass) / (aMass + bMass)) * bForce.x;
	bfVY = ((2.f * aMass) / (aMass + bMass)) * aForce.y
		+ ((bMass - aMass) / (aMass + bMass)) * bForce.y;
	bfVZ = ((2.f * aMass) / (aMass + bMass)) * aForce.z
		+ ((bMass - aMass) / (aMass + bMass)) * bForce.z;

	int multiple = 0.3;

	pFirst->GetPhysics().m_Velocity.x = afVX* multiple;
	pFirst->GetPhysics().m_Velocity.y = afVY * multiple;
	pFirst->GetPhysics().m_Velocity.z = afVZ * multiple;

	pSecond->GetPhysics().m_Velocity.x = bfVX * multiple;
	pSecond->GetPhysics().m_Velocity.y = bfVY * multiple;
	pSecond->GetPhysics().m_Velocity.z = bfVZ * multiple;
}

void Collision::ProcessCollision(Bullet* pFirst, Actor* pSecond)
{
	float aMass = pFirst->m_Physics->m_Mass;
	XMFLOAT3 aForce = pFirst->m_Physics->m_Velocity;
	float bMass = pSecond->GetPhysics().m_Mass;
	XMFLOAT3 bForce = pSecond->GetPhysics().m_Velocity;

	float afVX = 0, afVY = 0, afVZ = 0;
	float bfVX = 0, bfVY = 0, bfVZ = 0;

	afVX = ((aMass - bMass) / (aMass + bMass)) * aForce.x
		+ ((2.f * bMass) / (aMass + bMass)) * bForce.x;
	afVY = ((aMass - bMass) / (aMass + bMass)) * aForce.y
		+ ((2.f * bMass) / (aMass + bMass)) * bForce.y;
	afVZ = ((aMass - bMass) / (aMass + bMass)) * aForce.z
		+ ((2.f * bMass) / (aMass + bMass)) * bForce.z;

	bfVX = ((2.f * aMass) / (aMass + bMass)) * aForce.x
		+ ((bMass - aMass) / (aMass + bMass)) * bForce.x;
	bfVY = ((2.f * aMass) / (aMass + bMass)) * aForce.y
		+ ((bMass - aMass) / (aMass + bMass)) * bForce.y;
	bfVZ = ((2.f * aMass) / (aMass + bMass)) * aForce.z
		+ ((bMass - aMass) / (aMass + bMass)) * bForce.z;

	int multiple = 0.6;

	pFirst->m_Physics->m_Velocity.x = afVX * multiple;
	pFirst->m_Physics->m_Velocity.y = afVY * multiple;
	pFirst->m_Physics->m_Velocity.z = afVZ * multiple;

	pSecond->GetPhysics().m_Velocity.x = bfVX * multiple;
	pSecond->GetPhysics().m_Velocity.y = bfVY * multiple;
	pSecond->GetPhysics().m_Velocity.z = bfVZ * multiple;
}

bool Collision::BBOverlapTest(Actor* pFirst, Actor* pSecond)
{
	XMFLOAT3 firstMin;
	XMFLOAT3 firstMax;
	XMFLOAT3 secondMin;
	XMFLOAT3 secondMax;

	// bounding box
	firstMin.x = pFirst->GetPhysics().m_Position.x - pFirst->GetPhysics().m_Volume.x / 2;
	firstMin.y = pFirst->GetPhysics().m_Position.y - pFirst->GetPhysics().m_Volume.y / 2;
	firstMin.z = pFirst->GetPhysics().m_Position.z - pFirst->GetPhysics().m_Volume.z / 2;
	firstMax.x = pFirst->GetPhysics().m_Position.x + pFirst->GetPhysics().m_Volume.x / 2;
	firstMax.y = pFirst->GetPhysics().m_Position.y + pFirst->GetPhysics().m_Volume.y / 2;
	firstMax.z = pFirst->GetPhysics().m_Position.z + pFirst->GetPhysics().m_Volume.z / 2;

	secondMin.x = pSecond->GetPhysics().m_Position.x - pSecond->GetPhysics().m_Volume.x / 2;
	secondMin.y = pSecond->GetPhysics().m_Position.y - pSecond->GetPhysics().m_Volume.y / 2;
	secondMin.z = pSecond->GetPhysics().m_Position.z - pSecond->GetPhysics().m_Volume.z / 2;
	secondMax.x = pSecond->GetPhysics().m_Position.x + pSecond->GetPhysics().m_Volume.x / 2;
	secondMax.y = pSecond->GetPhysics().m_Position.y + pSecond->GetPhysics().m_Volume.y / 2;
	secondMax.z = pSecond->GetPhysics().m_Position.z + pSecond->GetPhysics().m_Volume.z / 2;


	if (firstMin.x > secondMax.x)
		return false;
	if (firstMax.x < secondMin.x)
		return false;

	if (firstMin.y > secondMax.y)
		return false;
	if (firstMax.y < secondMin.y)
		return false;

	if (firstMin.z > secondMax.z)
		return false;
	if (firstMax.z < secondMin.z)
		return false;

	return true;
}

bool Collision::BBOverlapTest(Bullet* pFirst, Actor* pSecond)
{
	XMFLOAT3 firstMin;
	XMFLOAT3 firstMax;
	XMFLOAT3 secondMin;
	XMFLOAT3 secondMax;

	// bounding box
	firstMin.x = pFirst->m_Physics->m_Position.x - pFirst->m_Physics->m_Volume.x / 2;
	firstMin.y = pFirst->m_Physics->m_Position.y - pFirst->m_Physics->m_Volume.y / 2;
	firstMin.z = pFirst->m_Physics->m_Position.z - pFirst->m_Physics->m_Volume.z / 2;
	firstMax.x = pFirst->m_Physics->m_Position.x + pFirst->m_Physics->m_Volume.x / 2;
	firstMax.y = pFirst->m_Physics->m_Position.y + pFirst->m_Physics->m_Volume.y / 2;
	firstMax.z = pFirst->m_Physics->m_Position.z + pFirst->m_Physics->m_Volume.z / 2;

	secondMin.x = pSecond->GetPhysics().m_Position.x - pSecond->GetPhysics().m_Volume.x / 2;
	secondMin.y = pSecond->GetPhysics().m_Position.y - pSecond->GetPhysics().m_Volume.y / 2;
	secondMin.z = pSecond->GetPhysics().m_Position.z - pSecond->GetPhysics().m_Volume.z / 2;
	secondMax.x = pSecond->GetPhysics().m_Position.x + pSecond->GetPhysics().m_Volume.x / 2;
	secondMax.y = pSecond->GetPhysics().m_Position.y + pSecond->GetPhysics().m_Volume.y / 2;
	secondMax.z = pSecond->GetPhysics().m_Position.z + pSecond->GetPhysics().m_Volume.z / 2;


	if (firstMin.x > secondMax.x)
		return false;
	if (firstMax.x < secondMin.x)
		return false;

	if (firstMin.y > secondMax.y)
		return false;
	if (firstMax.y < secondMin.y)
		return false;

	if (firstMin.z > secondMax.z)
		return false;
	if (firstMax.z < secondMin.z)
		return false;

	return true;
}
