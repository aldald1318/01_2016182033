#include "stdafx.h"
#include "IGameObject.h"

// 어떤 오브젝트인지를 나타내는 수를 표현해줘야함
// instancing 개수
unsigned int currentObjectID = 0;

IGameObject::IGameObject() : 
	_objectID(currentObjectID++)
{
	obj_index = _objectID;
}

IGameObject::~IGameObject()
{
}

size_t IGameObject::GetObjectID() const
{
	return _objectID;
}

size_t IGameObject::GetObjectType() const
{
	return _objectType;
}
