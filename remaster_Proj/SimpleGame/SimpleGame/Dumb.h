#pragma once
#include "Actor.h"
#include "Pawn.h"

class Character;
class Dumb : public Pawn
{
public:
	explicit Dumb();
	virtual ~Dumb();

public:
	virtual void Initialize() override;
	virtual void Release() override;

public:
	void FollowCharacter(Character* player, const float deltaT);

public:
	int m_speed;

};

