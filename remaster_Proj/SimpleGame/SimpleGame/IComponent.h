#pragma once
class IComponent abstract
{
private:
	size_t _possedActorID;

public:
	explicit IComponent() = default;
	virtual ~IComponent() = default;

public:
	void SetPossedActorID(size_t possedID);
	size_t GetPossedActorID() const;
};

