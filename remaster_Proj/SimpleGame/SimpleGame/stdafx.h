#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>

#include <iostream>
#include <stack>
#include <queue>
#include <vector>
#include <map>
#include <unordered_map>
#include <list>
#include <algorithm>

#include "Dependencies\glew.h"
#include "Dependencies\freeglut.h"

#include <DirectXMath.h>

using namespace std;

#include "extern.h"
#include "Structures.h"

#define SAFE_DELETE_PTR(ptr)	\
{								\
	if (ptr != nullptr)			\
	{							\
		delete ptr;				\
		ptr = nullptr;			\
	}							\
}	