#include "stdafx.h"
#include "Issac.h"
#include "SceneManager.h"
#include "TitleScene.h"
#include "GameplayScene.h"
#include "Renderer.h"
#include "Map.h"
#include "UI.h"
#include "Bullet.h"
#include "Sound.h"

float shootcooltime = 0.f;

Issac::Issac() :
	m_SceneManager(nullptr),
	m_Renderer(nullptr)
{
}

Issac::~Issac()
{
}

void Issac::Startup(void)
{
	LoadSounds();
	m_Renderer = Renderer::GetApp();
	if (!m_Renderer->IsInitialized())
	{
		std::cout << "Renderer could not be initialized.. \n";
	}
	m_SceneManager = SceneManager::GetApp();
	m_SceneManager->ChangeScenes<TitleScene>(false);
	// Resources 생성
	// Mesh
	// Texture ,, 
	// 게임에 필요한 모든 리소스 빌드

}

void Issac::Cleanup(void)
{
	SAFE_DELETE_PTR(m_Renderer);
}

void Issac::Update(float deltaT)
{
	shootcooltime -= deltaT;
	GarbageCollection();
	//cout << m_Bullets.size();

	for (auto& physics : _physicsComponents) {
		physics.update(deltaT);
	}
	
	for (auto& graphics : _graphicsComponents)
		graphics.Update();

	if (m_SceneManager != nullptr)
		m_SceneManager->UpdateScene(deltaT);

	for (auto& phy : m_Bullets)
	{
		phy->m_Physics->update(deltaT);
	}

	//// Object Update
	//for (auto& obj : _allObjects)
	//	if (obj) obj->Update(deltaT);
}

void Issac::RenderScene(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearDepth(1.f);
	glClearColor(0.0f, 0.3f, 0.3f, 1.0f);

	// 1Union 10m
	for (auto& obj : _allObjects) {
		if(obj != nullptr) obj->PreRender(1);
	}

	for (auto& grh : _graphicsComponents)
		grh.Render(m_Renderer);

	for (auto& bullet : m_Bullets) {
		bullet->PreRender(1);
		bullet->m_Graphics->Render(m_Renderer);
	}


	if(m_Map)
		m_Map->Render();

	if (m_UI)
		m_UI->Render();

	glutSwapBuffers();
}

void Issac::ClearAll()
{

	//for (size_t i = 0; i < _allObjects.size(); ++i)
	//{
	//	_allObjects[i]->Release();
	//	SAFE_DELETE_PTR(_allObjects[i]);
	//}
	_allObjects.clear();
	_dicObjects.clear();

	// component,, 등등 다초기화
	_physicsComponents.clear();
	_graphicsComponents.clear();

	m_Bullets.clear();
	_Textures.clear();

	if(m_UI)
		delete m_UI;
	if(m_Map)
		m_Map = NULL;

	currentObjectID = 0;
}

void Issac::Create(PhysicsComponent** tempPhysics)
{
	Create(_physicsComponents, tempPhysics);
}

void Issac::Create(GraphicsComponent** tempGraphics)
{
	Create(_graphicsComponents, tempGraphics);
}

void Issac::GarbageCollection()
{
	//m_Bullets.remove();
	m_Bullets.remove_if([](Bullet* b){
		if (b->m_Physics->m_Velocity.x == 0 &&
			b->m_Physics->m_Velocity.y == 0 &&
			b->m_Physics->m_Velocity.z <= 0.00000001)
		{
			delete b;
			return true;
		}

		return false;
		});
}

void Issac::AddTexture(const std::string name, char* imagePath)
{
	int texId = m_Renderer->GenPngTexture(imagePath);
	_Textures.emplace(name, texId);
}

int Issac::GetTexture(const std::string name)
{
	if(_Textures[name])
		return _Textures[name];
	return -1;
}

void Issac::LoadSounds()
{
	int soundkey;
	soundkey = g_sound->CreateBGSound("./Resources/Sounds/main.mp3");
	_allSounds.emplace("title", soundkey);

	soundkey = g_sound->CreateBGSound("./Resources/Sounds/gameplay.mp3");
	_allSounds.emplace("gameplay", soundkey);

	soundkey = g_sound->CreateBGSound("./Resources/Sounds/event.mp3");
	_allSounds.emplace("event", soundkey);

	soundkey = g_sound->CreateShortSound("./Resources/Sounds/Explosion.mp3");
	_allSounds.emplace("attack", soundkey);

	soundkey = g_sound->CreateShortSound("./Resources/Sounds/die.mp3");
	_allSounds.emplace("die", soundkey);

	//CreateSound("title", "./Resources/Sounds/menu.mp3");
	//CreateSound("title", "./Resources/Sounds/Win.mp3");
}