#pragma once

class PhysicsComponent;
class GraphicsComponent;
class Bullet
{
public:
	explicit Bullet();
	virtual ~Bullet();

public:
	virtual void Initialize();
	virtual void Release();

	void PreRender(const float Interpolation);

	void Update();

public:
	PhysicsComponent* m_Physics;
	GraphicsComponent* m_Graphics;

	
};

