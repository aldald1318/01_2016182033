#pragma once
#include "IGameApp.h"
#include "Singleton.h"
#include "IGameObject.h"
#include "PhysicsComponent.h"
#include "GraphicsComponent.h"
#include "State.h"

extern float shootcooltime;

class UI;
class Map;
class SceneManager;
class Issac : public IGameApp, public Singleton<Issac>
{
public:
	explicit Issac();
	virtual ~Issac();

public:
	virtual void Startup(void) override;
	virtual void Cleanup(void) override;
	virtual void Update(float deltaT) override;
	virtual void RenderScene(void) override;

public:
	/* Create
	@ 컴포넌트 추가 메서드
	@ 컴포넌트 포인터를 반환함
	*/
	template <class T>
	T* Create()
	{
		T* tempPtr = nullptr;
		Create(&tempPtr);
		return tempPtr;
	}

	template <class TObject>
	TObject* AddObject()
	{
		TObject* obj = new TObject;
		cout << "Obj-" << obj->GetObjectID() << " Create" << endl;
		cout << "Obj-" << obj->obj_index << " Create" << endl;
		_allObjects.emplace_back(obj);
		_dicObjects.emplace(obj->GetObjectID(), obj);

		return dynamic_cast<TObject*>(obj);
	}

	void ClearAll();
	//  DeleteObject
	// 각 오브젝트들은 몇번째 인덱스인지 저정함
	// id_type이 인덱스


private:
	/* Sub Create Component
	@ Physics & Graphics 생성 overload함수
	*/
	void Create(PhysicsComponent** tempPhysics);
	void Create(GraphicsComponent** tempGraphics);
	//void Create(Controller** tempController);

	template <class T>
	void Create(std::deque<T>& ServiceContainer, T** OutService)
	{
		ServiceContainer.emplace_back();
		if (OutService)
			*OutService = &ServiceContainer.back();
	}

public:
	SceneManager*	m_SceneManager;
	Renderer*		m_Renderer;

public:
	/* Objects
	@ 각각의 오브젝트는 클래스는 자신만의 고유한 ID를 가짐
	@ ID는 클래스 해쉬코드 + 같은 클래스 개수
	@ 순회용도 vector와 검색용도 map 컨테이너가 있다.
	*/
	std::vector<IGameObject*>		_allObjects;
	std::map<size_t, IGameObject*>	_dicObjects;

	/* Components
	@ 최적화 패턴이 사용됨
	*/
	std::deque<PhysicsComponent>			_physicsComponents;
	std::deque<GraphicsComponent>			_graphicsComponents;

public:
	void GarbageCollection();

	std::list<Bullet*> m_Bullets;

public:
	void AddTexture(const std::string name, char* imagePath);
	int GetTexture(const std::string name);

	/* Resources */
	std::unordered_map<std::string, int>						_Textures;
public:
	/*시간이 없는 관계로*/
	UI* m_UI;
	Map* m_Map;

public:

	void LoadSounds();
	unordered_multimap<std::string, int> _allSounds;
};