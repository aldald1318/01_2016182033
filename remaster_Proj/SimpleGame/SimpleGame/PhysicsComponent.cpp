#include "stdafx.h"
#include "PhysicsComponent.h"

using namespace DirectX;

PhysicsComponent::PhysicsComponent()
{
	m_PrevPosition = { 0.f,0.f,0.f };
	m_Position = { 0.f,0.f,0.f };
	m_Velocity = { 0.f,0.f,0.f };
	m_Acc = { 0.f,0.f,0.f };
	m_Volume = { 0.f,0.f,0.f };
	m_GravityMultipler = 1.f;
	m_Mass = 1.f;
	m_Friction = 1.f;
}

PhysicsComponent::~PhysicsComponent()
{
	cout << GetPossedActorID() << " physics die" << endl;
}

void PhysicsComponent::update(const float& deltaTime)
{
	/* --------------------------------- Interpolation ---------------------------------------- */
	m_PrevPosition = m_Position;

	XMVECTOR pos = XMLoadFloat3(&m_Position);
	XMVECTOR vel = XMLoadFloat3(&m_Velocity);
	XMVECTOR acc = XMLoadFloat3(&m_Acc);

	/* ---------------------------------- Gravity --------------------------------------------- */
	float gravityForce = GRAVITY * m_GravityMultipler; /*mass*/

	acc = XMVectorAdd(acc, { 0.f,0.f, gravityForce });

	/* --------------------------------- Friction --------------------------------------------- */

	float frictionAccel = m_Friction * gravityForce;
	float frictionSpeed = frictionAccel * deltaTime;

	XMVECTOR frictionVelocity = XMVectorScale(XMVector3Normalize(vel), frictionSpeed);
	XMVECTOR prevVelocity = vel;

	vel = XMVectorAdd(vel, frictionVelocity);

	XMVECTOR FrictionError = Evaluate(GreaterOrEqual
	(
		XMVectorMultiply(vel, prevVelocity),
		XMVectorZero()
	));

	vel = XMVectorMultiply(vel, FrictionError);

	/* ------------------------------- Update Position ----------------------------------------- */
	vel = XMVectorAdd(vel, XMVectorScale(acc, deltaTime));
	pos = XMVectorAdd(pos, XMVectorScale(vel, deltaTime));
	acc = XMVectorZero();

	if (XMVectorGetZ(pos) < 0.f)
	{
		pos = XMVectorSetZ(pos, 0.f);
		vel = XMVectorSetZ(vel, 0.f);
	}

	/* ---------------------------------- Apply Physics -------------------------------------- */
	XMStoreFloat3(&m_Acc,acc);
	XMStoreFloat3(&m_Velocity, vel);
	XMStoreFloat3(&m_Position, pos);
}

void PhysicsComponent::ApplyForce(XMVECTOR force)
{
	XMVECTOR addAcc = XMVectorScale(force, 1.f / m_Mass);
	XMStoreFloat3(&m_Acc,XMVectorAdd(XMLoadFloat3(&m_Acc),addAcc));
}

void PhysicsComponent::SetPosition(DirectX::XMVECTOR pos)
{
	XMStoreFloat3(&m_Position, pos);
	m_PrevPosition = m_Position;
}

void PhysicsComponent::SetVelocity(XMFLOAT3 vel)
{
	m_Velocity = vel;
}

void PhysicsComponent::SetAcceleration(XMFLOAT3 acc)
{
	m_Acc = acc;
}

void PhysicsComponent::SetVolume(XMFLOAT3 vol)
{
	m_Volume = vol;
}

void PhysicsComponent::SetMass(float mass)
{
	m_Mass = mass;
}

void PhysicsComponent::SetFriction(float friction)
{
	m_Friction = friction;
}

void PhysicsComponent::SetDirection(int dir)
{
	m_Direction = dir;
}
