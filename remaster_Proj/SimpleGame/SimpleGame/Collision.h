#pragma once

class Actor;
class Bullet;

namespace Collision
{
	bool IsOverlap(Actor* a1, Actor* a2, int type = 0);
	bool IsOverlap(Bullet* a1, Actor* a2, int type = 0);

	void ProcessCollision(Actor* a1, Actor* a2);
	void ProcessCollision(Bullet* a1, Actor* a2);

	bool BBOverlapTest(Actor* a1, Actor* a2);
	bool BBOverlapTest(Bullet* a1, Actor* a2);
}




