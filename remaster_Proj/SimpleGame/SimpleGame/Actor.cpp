#include "stdafx.h"
#include "Actor.h"
#include "IComponent.h"
#include "Issac.h"
#include "PhysicsComponent.h"
#include "GraphicsComponent.h"

Actor::Actor()
{
	// Create Physics & Component
	m_Physics = Issac::GetApp()->Create<PhysicsComponent>();
	m_Graphics = Issac::GetApp()->Create<GraphicsComponent>();
	m_Graphics->isObject = true;
	
	m_Physics->obj_index = obj_index;
	m_Graphics->obj_index = obj_index;
}

Actor::~Actor()
{
}

void Actor::send(int message)
{
}

void Actor::PreRender(float Interpolation)
{
	DirectX::XMVECTOR position = DirectX::XMVectorAdd
	(
		DirectX::XMVectorScale(XMLoadFloat3(&m_Physics->m_Position), Interpolation),
		DirectX::XMVectorScale(XMLoadFloat3(&m_Physics->m_PrevPosition), 1.f - Interpolation)
	);

	DirectX::XMVECTOR volume = DirectX::XMVectorScale(XMLoadFloat3(&m_Physics->m_Volume),Interpolation);

	DirectX::XMStoreFloat3(&m_Graphics->m_Position, position);
	DirectX::XMStoreFloat3(&m_Graphics->m_Volume, volume);
}

PhysicsComponent& Actor::GetPhysics()
{
	return *m_Physics;
}

GraphicsComponent& Actor::GetGraphics()
{
	return *m_Graphics;
}
