#include "stdafx.h"
#include "UI.h"
#include "Renderer.h"
#include "Issac.h"

UI::UI()
{
}

UI::~UI()
{
}

void UI::Render()
{
	Renderer::GetApp()->DrawTextureRectAnim(0, -(float)Graphics::g_ClientHeight/2, 0, (float)Graphics::g_ClientWidth, (float)Graphics::g_ClientHeight, 0, 1, 1, 1, 1, m_TextureID,1,1,0,0);
}

void UI::SetTexture(std::string id)
{
	m_TextureID = Issac::GetApp()->_Textures[id];
}
