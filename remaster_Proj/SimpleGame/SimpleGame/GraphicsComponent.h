#pragma once
#include "IComponent.h"

#define UPDATE_TIME 0.008

enum SpriteLayer
{
	//None = 0xFF,
	Up = 0x03,
	Down = 0x00,
	Left = 0x01,
	Right = 0x02,
};

enum SpriteType
{
	Linear,
	Grid,
};

class Renderer;
class PhysicsComponent;
class GraphicsComponent : public IComponent
{
public:
	explicit GraphicsComponent();
	virtual ~GraphicsComponent();

public:
	virtual void Render(Renderer* renderer);

	void Update();
	void LinearUpdate();
	void GridUpdate();
	void NextFrame();

public:
	void SetPosition(DirectX::XMVECTOR pos);
	void SetTexture(const std::string texName);

	void SetTotal(int column, int row);
	void SetCurrent(int column, int row);

	void SetFrameRate(const float frameRate);
	void SetLayer(SpriteLayer layer);

public:
	DirectX::XMFLOAT3 m_Position;
	DirectX::XMFLOAT3 m_Volume;
	DirectX::XMFLOAT4 m_Color;

public:
	int m_Texture;
	
	/* Total */
	XMUINT2 m_Total;
	XMUINT2 m_Current;

	int m_Row; /*����*/
	int m_Column; /*����*/

	float m_FrameRate;
	float m_CurFrame;
	SpriteLayer m_Layer;

public:
	bool isObject = false;
	float HPpercent;
	XMFLOAT3 HPHeight;
	XMUINT2 HPSize;

public:
	int obj_index;
};

