#pragma once

class Map
{
public:
	explicit Map();
	virtual ~Map();

public:
	void Render();
	void SetPosition(const XMFLOAT3 playerPos) { m_Position = playerPos; };
	void SetTexture(std::string id);

private:
	int m_TextureID;
	XMFLOAT3 m_Position;
};

