#include "stdafx.h"
#include "SceneManager.h"
#include "Scene.h"

SceneManager::SceneManager()
{
}

SceneManager::~SceneManager()
{
	for (size_t i = 0; i < m_Scenes.size(); ++i)
		PopScene();
}

bool SceneManager::PopScene()
{
	if (m_Scenes.size() <= 1)
		return false;

	Scene* pScene = m_Scenes.top();
	if (pScene)
	{
		pScene->Exit();
		delete pScene;
	}
	m_Scenes.pop();
	return true;
}

void SceneManager::UpdateScene(const float& DeltaTime)
{
	m_Scenes.top()->Update(DeltaTime);
}

Scene* SceneManager::GetCurScene() const
{
	return m_Scenes.top();
}
