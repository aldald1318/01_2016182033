#pragma once

/* Scene
@ Engine의 설계도
@ 그릴 오브젝트들이 무엇인지를 설계한다.
@ 오브젝트 생성 및 배치
*/
class Actor;
class Pawn;
class Character;
class Scene abstract
{
public:
	explicit Scene();
	virtual ~Scene();

public:
	virtual bool	Enter() = 0;
	virtual void	Exit() = 0;

	virtual void	Update(const float& fDeltaTime) = 0;

public:
	Actor*		CreateActor();
	Pawn*		CreatePawn();
	Character*	CreateCharacter();
};

