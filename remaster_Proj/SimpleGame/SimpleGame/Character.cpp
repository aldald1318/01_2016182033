#include "stdafx.h"
#include "Character.h"

Character::Character()
{
	IGameObject::CreateObjectID<Character>();
	m_Physics->SetPossedActorID(_objectID);
	m_Graphics->SetPossedActorID(_objectID);
}

Character::~Character()
{
}

void Character::Initialize()
{

}

void Character::Release()
{
}

#include "Renderer.h"
void Character::Update(const float DeltaT)
{
	Pawn::Update(DeltaT);
	Renderer::GetApp()->SetCameraPos(m_Physics->m_Position.x, m_Physics->m_Position.y);
}