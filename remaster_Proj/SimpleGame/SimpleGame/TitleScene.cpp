#include "stdafx.h"
#include "TitleScene.h"
#include "Issac.h"
#include "Dumb.h"
#include "Input.h"
#include "Command.h"
#include "GameplayScene.h"
#include "UI.h"
#include "Renderer.h"
#include "Sound.h"

TitleScene::TitleScene()
{
}

TitleScene::~TitleScene()
{
}

bool TitleScene::Enter()
{
	Renderer::GetApp()->SetCameraPos(0, 0);

	LoadResources();
	LoadSounds();

	Issac::GetApp()->m_UI = new UI;
	Issac::GetApp()->m_UI->SetTexture("title");


	return false;
}

void TitleScene::Exit()
{
}

#include "SceneManager.h"
void TitleScene::Update(const float& fDeltaTime)
{
	if (isPressed(VK_RETURN))
	{
		delete Issac::GetApp()->m_UI;
		Issac::GetApp()->m_UI = nullptr;
		SceneManager::GetApp()->ChangeScenes<GameplayScene>(false);
	}
}

void TitleScene::LoadResources()
{
	Issac::GetApp()->AddTexture("title", "./Resources/title.png");
}

void TitleScene::LoadSounds()
{
	g_sound->StopBGSound(Issac::GetApp()->_allSounds.find("gameplay")->second);
	g_sound->PlayBGSound(Issac::GetApp()->_allSounds.find("title")->second, false, 1);
}

