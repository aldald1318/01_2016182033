#include "stdafx.h"
#include "Bullet.h"
#include "Character.h"
#include "State.h"
#include "GraphicsComponent.h"
#include "PhysicsComponent.h"

Bullet::Bullet()
{
	Initialize();
}

Bullet::~Bullet()
{
	Release();
}

void Bullet::Initialize()
{
	m_Graphics = new GraphicsComponent;
	m_Physics = new PhysicsComponent;
}

void Bullet::Release()
{
}

void Bullet::PreRender(const float Interpolation)
{
	DirectX::XMVECTOR position = DirectX::XMVectorAdd
	(
		DirectX::XMVectorScale(XMLoadFloat3(&m_Physics->m_Position), Interpolation),
		DirectX::XMVectorScale(XMLoadFloat3(&m_Physics->m_PrevPosition), 1.f - Interpolation)
	);

	DirectX::XMVECTOR volume = DirectX::XMVectorScale(XMLoadFloat3(&m_Physics->m_Volume), Interpolation);

	DirectX::XMStoreFloat3(&m_Graphics->m_Position, position);
	DirectX::XMStoreFloat3(&m_Graphics->m_Volume, volume);
}


void Bullet::Update()
{
}
