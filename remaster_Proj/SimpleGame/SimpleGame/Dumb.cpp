#include "stdafx.h"
#include "Dumb.h"
#include "Character.h"
#include "State.h"

Dumb::Dumb()
{
	IGameObject::CreateObjectID<Dumb>();
	m_Physics->SetPossedActorID(_objectID);
	m_Graphics->SetPossedActorID(_objectID);
}

Dumb::~Dumb()
{
	cout << "Dumb Delete" << endl;
}

void Dumb::Initialize()
{
}

void Dumb::Release()
{
}

void Dumb::FollowCharacter(Character* player, const float deltaT)
{
	Pawn::Update(deltaT);

	XMVECTOR myPos = XMLoadFloat3(&m_Physics->m_Position);
	XMVECTOR targetPos = XMLoadFloat3(&player->GetPhysics().m_Position);
	XMVECTOR target = XMVectorSubtract(targetPos, myPos);
	target = XMVector3Normalize(target);
	
	m_Physics->ApplyForce(target);
	
	XMFLOAT3 tt;
	XMStoreFloat3(&tt,target);

	m_Physics->m_Position.x += tt.x * m_speed *deltaT;
	m_Physics->m_Position.y += tt.y* m_speed * deltaT;
	m_Physics->m_Position.z += tt.z* m_speed * deltaT;

}
