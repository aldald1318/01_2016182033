#include "stdafx.h"
#include "GraphicsComponent.h"
#include "Renderer.h"
#include "Issac.h"

GraphicsComponent::GraphicsComponent()
{
	/* Graphics Init Value
	*/
	m_Position = { 0.f,0.f,0.f };
	m_Volume = { 0.f,0.f,0.f };
	m_Color = { 1.f,1.f,1.f,1.f };
	m_CurFrame = 0;
	m_Current = { 0,0 };
	m_FrameRate = 0;
	//m_Layer = SpriteLayer::None;
	m_Texture = -1;
	m_Total = { 0, 0 };
	HPpercent = 100;
	HPSize = { 35, 10 };
}

GraphicsComponent::~GraphicsComponent()
{
}

void GraphicsComponent::Render(Renderer* renderer)
{
	if(isObject == true)
		renderer->DrawSolidRectGauge(m_Position.x, m_Position.y, m_Position.z,HPHeight.x,HPHeight.y,HPHeight.z, HPSize.x, HPSize.y, 1, 1, 0, 0, 1, HPpercent, false);

	renderer->DrawTextureRectAnim(m_Position.x, m_Position.y, m_Position.z, m_Volume.x, m_Volume.y, m_Volume.z, m_Color.x, m_Color.y, m_Color.z, m_Color.w, m_Texture,m_Total.x,m_Total.y,m_Current.x,m_Current.y);
}

void GraphicsComponent::Update()
{
	LinearUpdate();
}

void GraphicsComponent::LinearUpdate()
{
	if (m_Total.x <= m_CurFrame)
		m_CurFrame = 0;
	m_CurFrame += UPDATE_TIME * m_FrameRate;
	m_Current.x = static_cast<int>(m_CurFrame);
}

void GraphicsComponent::GridUpdate()
{
}

void GraphicsComponent::NextFrame()
{

}

void GraphicsComponent::SetPosition(DirectX::XMVECTOR pos)
{
	XMStoreFloat3(&m_Position, pos);
}

void GraphicsComponent::SetTexture(const std::string texName)
{
	m_Texture = Issac::GetApp()->GetTexture(texName);
}

void GraphicsComponent::SetTotal(int column, int row)
{
	m_Total.x = column;
	m_Total.y = row;
}

void GraphicsComponent::SetCurrent(int column, int row)
{
	m_Current.x = column;
	m_Current.y = row;
}

void GraphicsComponent::SetFrameRate(const float frameRate)
{
	m_FrameRate = frameRate;
}

void GraphicsComponent::SetLayer(SpriteLayer layer)
{
	m_Layer = layer;
	m_Current.y = m_Layer;
}
