#pragma once
#include "Singleton.h"

class Scene;
class SceneManager : public Singleton<SceneManager>
{
public:
	SceneManager();
	~SceneManager();

public:
	template<class NewScene>
	void ChangeScenes(bool bPopScene)
	{
		if (bPopScene)
			PopScene();
		m_Scenes.emplace(new NewScene);
		m_Scenes.top()->Enter();
	}
	bool	PopScene();
	Scene*	GetCurScene() const;

public:
	void	UpdateScene(/*TimeDelta*/const float& DeltaTime);

private:
	std::stack<Scene*> m_Scenes;
};

