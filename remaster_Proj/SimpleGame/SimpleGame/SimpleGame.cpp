/*
Copyright 2017 Lee Taek Hee (Korea Polytech University)

This program is free software: you can redistribute it and/or modify
it under the terms of the What The Hell License. Do it plz.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY.
*/

#include "stdafx.h"

#include "Issac.h"
#include "Sound.h"

Issac* app;
Sound* g_sound;
int g_prevTimeInMillisecond;

namespace Graphics
{
	const char* g_Appname = "Issac - by Taek";
	const size_t g_ClientWidth = 800;
	const size_t g_ClientHeight = 600;
}


void RenderScene(int elapsedTime)
{
	int currentTime = glutGet(GLUT_ELAPSED_TIME);
	elapsedTime = currentTime - g_prevTimeInMillisecond;
	g_prevTimeInMillisecond = currentTime;

	app->Update((float)elapsedTime / 1000);
	app->RenderScene();

	glutTimerFunc(16, RenderScene, 16);
}

void Idle(void)
{
}

void MouseInput(int button, int state, int x, int y)
{
}

int main(int argc, char **argv)
{
	// Initialize GL things
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(Graphics::g_ClientWidth, Graphics::g_ClientHeight);
	glutCreateWindow(Graphics::g_Appname);

	glewInit();
	if (glewIsSupported("GL_VERSION_3_0"))
	{
		std::cout << " GLEW Version is 3.0\n ";
	}
	else
	{
		std::cout << "GLEW 3.0 not supported\n ";
	}
	g_sound = new Sound;

	app = Issac::GetApp();
	app->Startup();



	glutDisplayFunc(Idle);
	glutIdleFunc(Idle);
	glutMouseFunc(MouseInput);
	//glutKeyboardFunc();

	g_prevTimeInMillisecond = glutGet(GLUT_ELAPSED_TIME);
	glutTimerFunc(16, RenderScene, 16);

	glutMainLoop();

	app->Cleanup();

    return 0;
}

