#pragma once
#include "stdafx.h"

template <typename Type>
class Singleton
{
protected:
	// 持失切 社瑚切 private
	Singleton() {};
	virtual ~Singleton() {};

public:
	static Type* GetApp()
	{
		if (m_pApp == nullptr)
			m_pApp = new Type;
		return m_pApp;
	};

	static void DestroyApp()
	{
		SAFE_DELETE_PTR(m_pApp);
	};

private:
	static Type* m_pApp;
};

template <typename Type> Type* Singleton<Type>::m_pApp = nullptr;