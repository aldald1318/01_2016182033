#pragma once

/* Object 생성 양식
IGameObject::CreateObjectID<Class>();
*/
extern unsigned int currentObjectID;

class IGameObject abstract
{
protected:
	size_t _objectID;
	size_t _objectType;

public:
	explicit IGameObject();
	virtual ~IGameObject();

public:
	virtual void	Initialize()=0;
	virtual void	Release()=0;
	virtual void	PreRender(float Interpolation) = 0;
	virtual void	Update(const float deltaT) {};

protected:
	/* CreateObjectID
	@ 새로운 오브젝트를 생성한다면
	@ 무조건 생성자에 이 함수를 호출하자.
	*/
	template<class VObject>
	void CreateObjectID()
	{
		_objectType = typeid(VObject).hash_code();
		_objectID += typeid(VObject).hash_code();
	}

public:
	size_t GetObjectID() const;
	size_t GetObjectType() const;
	size_t obj_index = 0;
};

