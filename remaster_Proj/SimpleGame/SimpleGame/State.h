#pragma once

class Pawn;
class State
{
public:
	explicit State() = default;
	virtual ~State() = default;

public:
	virtual void Enter(Pawn* pawn) = 0;
	virtual void Exit(Pawn* pawn) = 0;
	virtual void Update(Pawn* pawn) = 0;

public:
	void SetVK(int vk) { m_VK = vk; }

protected:
	int m_VK;
};

class IdleState : public State
{
public:
	explicit IdleState() = default;
	virtual ~IdleState() = default;

public:
	virtual void Enter(Pawn* pawn);
	virtual void Exit(Pawn* pawn);
	virtual void Update(Pawn* pawn);
};

class MoveState : public State
{
public:
	explicit MoveState() = default;
	virtual ~MoveState() = default;

public:
	virtual void Enter(Pawn* pawn);
	virtual void Exit(Pawn* pawn);
	virtual void Update(Pawn* pawn);
};

class JumpState : public State
{
public:
	explicit JumpState() = default;
	virtual ~JumpState() = default;

public:
	virtual void Enter(Pawn* pawn);
	virtual void Exit(Pawn* pawn);
	virtual void Update(Pawn * pawn);

private:
	float m_JumpRange;
};

class InAirState : public State
{
public:
	explicit InAirState() = default;
	virtual ~InAirState() = default;

public:
	virtual void Enter(Pawn* pawn);
	virtual void Exit(Pawn* pawn);
	virtual void Update(Pawn* pawn);

private:
	float m_OriginFriction;
};

class Bullet;
class ShootState : public State 
{
public:
	explicit ShootState() = default;
	virtual ~ShootState() = default;

public:
	virtual void Enter(Pawn * pawn);
	virtual void Exit(Pawn * pawn);
	virtual void Update(Pawn * pawn);

public:
	float speed = 700.f;
	float defaulttime = 0.2;
};

class DieState : public State
{
public:
	explicit DieState() = default;
	virtual ~DieState() = default;

public:
	virtual void Enter(Pawn * pawn);
	virtual void Exit(Pawn * pawn);
	virtual void Update(Pawn * pawn);

public:
	int Layer = -1;

	float deltaTime = 0;
};