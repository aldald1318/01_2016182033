#include "stdafx.h"
#include "IComponent.h"

void IComponent::SetPossedActorID(size_t possedID)
{
	_possedActorID = possedID;
}

size_t IComponent::GetPossedActorID() const
{
	return _possedActorID;
}
