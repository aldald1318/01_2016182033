#include "stdafx.h"
#include "Pawn.h"
#include "Controller.h"
#include "State.h"

Pawn::Pawn()
{
	ChangeState<IdleState>(false, -1);
}

Pawn::~Pawn()
{
	while (PopState()) {}
	delete m_States.top(); m_States.top() = nullptr;
	m_States.pop();

	Release();
}

void Pawn::Initialize()
{
}

void Pawn::Release()
{
	delete m_Controller;

	for (auto& iter = Issac::GetApp()->_physicsComponents.begin(); iter != Issac::GetApp()->_physicsComponents.end(); ++iter)
	{
		if (iter->GetPossedActorID() == _objectID) {
			Issac::GetApp()->_physicsComponents.erase(iter);
			break;
		}
	}

	for (auto& iter = Issac::GetApp()->_graphicsComponents.begin(); iter != Issac::GetApp()->_graphicsComponents.end(); ++iter)
	{
		if (iter->GetPossedActorID() == _objectID) {
			Issac::GetApp()->_graphicsComponents.erase(iter);
			break;
		}
	}

	m_Physics = nullptr;
	m_Graphics = nullptr;
}

void Pawn::Update(const float DeltaT)
{
	if (m_Controller) m_Controller->update(DeltaT);
	m_States.top()->Update(this);
}

bool Pawn::PopState()
{
	if (m_States.size() <= 1) return false;
	m_States.top()->Exit(this);
	delete m_States.top(); m_States.top() = nullptr;
	m_States.pop();
	return true;
}

void Pawn::SetController()
{
	m_Controller = new PlayerController(this);
}

PlayerController* Pawn::GetController()
{
	if (!m_Controller) return nullptr;
	return m_Controller;
}

