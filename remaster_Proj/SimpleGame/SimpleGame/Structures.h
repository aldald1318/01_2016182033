#pragma once

typedef struct fpoint3
{
	float x;
	float y;
	float z;

}FPOINT3;

typedef struct fpoint4
{
	float x;
	float y;
	float z;
	float w;

}FPOINT4;