#pragma once
#include "IGameObject.h"
#include "Issac.h"
#include "PhysicsComponent.h"
#include "GraphicsComponent.h"

/* Actor 생성 양식
IGameObject::CreateObjectID<Class>();
m_Physics->SetPossedActorID(_objectID);
m_Graphics->SetPossedActorID(_objectID);
*/

/* Actor
@ 정적인 오브젝트 & 인스턴싱 오브젝트
@ Physics / Graphics 를 소유함
*/
class Actor : public IGameObject
{
public:
	explicit Actor();
	virtual ~Actor();
	virtual void send(int message);

public:
	virtual void Initialize() = 0;
	virtual void Release() = 0;

public:
	virtual void PreRender(float Interpolation);

public:
	PhysicsComponent& GetPhysics();
	GraphicsComponent& GetGraphics();

protected:
	PhysicsComponent* m_Physics;
	GraphicsComponent* m_Graphics;
};

