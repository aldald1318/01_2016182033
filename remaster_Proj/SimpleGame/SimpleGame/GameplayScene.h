#pragma once
#include "Scene.h"

class Map;
class Dumb;
class Character;
class GameplayScene : public Scene
{
public:
	explicit GameplayScene();
	virtual ~GameplayScene();

public:
	virtual bool Enter() override;
	virtual void Exit() override;

	// ����
	virtual void Update(const float& fDeltaTime) override;

public:
	void LoadResources();
	void CreateCharacter();
	void CreateCommands();
	void LoadSounds();


private:
	Character* m_Player;

	Dumb* m_Captine;
	Dumb* m_Thor;

	Dumb* m_Captine2;
	Dumb* m_Thor2;

	Dumb* m_Captine3;
	Dumb* m_Thor3;

	bool EventWinner = false;
	int EventNum = 6;
	
	Dumb* m_Hulk;

	float gamefinishtime = 0.f;
	bool isKKK = false;


	bool nextScene1 = false;
	Dumb* gameover;
};

