#include "stdafx.h"
#include "State.h"
#include "Pawn.h"
#include "Controller.h"
#include "Bullet.h"
#include "Dumb.h"
#include "Sound.h"

void IdleState::Enter(Pawn* pawn)
{
	pawn->GetGraphics().SetFrameRate(0.f);
	pawn->GetGraphics().SetCurrent(1, 0);
}

void IdleState::Exit(Pawn* pawn)
{
}

void IdleState::Update(Pawn* pawn)
{
	pawn->GetGraphics().SetCurrent(1, 0);

	if (pawn->GetPhysics().m_Velocity.z == 0)
		pawn->ChangeState<MoveState>(true, -1);
	else
		pawn->ChangeState<InAirState>(true,-1);
}

void MoveState::Enter(Pawn* pawn)
{
	pawn->GetGraphics().SetFrameRate(20.f);
}

void MoveState::Exit(Pawn* pawn)
{

}

void MoveState::Update(Pawn* pawn)
{
	switch (m_VK)
	{
	case VK_LEFT:
		pawn->GetGraphics().SetLayer(SpriteLayer::Left);
		break;
	case VK_RIGHT:
		pawn->GetGraphics().SetLayer(SpriteLayer::Right);
		break;
	case VK_DOWN:
		pawn->GetGraphics().SetLayer(SpriteLayer::Down);
		break;
	case VK_UP:
		pawn->GetGraphics().SetLayer(SpriteLayer::Up);
		break;
	default:
		break;
	}

	XMFLOAT3 pawn_vel = pawn->GetPhysics().m_Velocity;
	if (pawn_vel.x == 0 &&
		pawn_vel.y == 0 &&
		pawn_vel.z == 0)
		pawn->ChangeState<IdleState>(true, -1);

	else if(pawn_vel.z != 0)
		pawn->ChangeState<InAirState>(true, -1);
}

void JumpState::Enter(Pawn* pawn)
{
	m_JumpRange = 30000;
}
void JumpState::Exit(Pawn* pawn)
{
	pawn->GetPhysics().m_Friction = 40;
	pawn->GetPhysics().ApplyForce({ 0,0,m_JumpRange });
}

void JumpState::Update(Pawn* pawn)
{
}

void InAirState::Enter(Pawn* pawn)
{
	// 각 상태마다 필요한 값들이 있다.
	// state friction
	m_OriginFriction = pawn->GetPhysics().m_Friction;
	pawn->GetPhysics().m_Friction = 40;
}

void InAirState::Exit(Pawn* pawn)
{
 	pawn->GetPhysics().m_Friction = 50;
}

void InAirState::Update(Pawn* pawn)
{
	pawn->GetPhysics().m_Friction = 40;
	pawn->GetPhysics().ApplyForce({ 0,0,-45000 });

	XMFLOAT3 pawn_vel = pawn->GetPhysics().m_Velocity;
	if (pawn_vel.x == 0 &&
		pawn_vel.y == 0 &&
		pawn_vel.z == 0)
		pawn->ChangeState<IdleState>(true, -1);
	else if (pawn_vel.z == 0) {
		pawn->ChangeState<MoveState>(true, -1);
	}
}

void ShootState::Enter(Pawn* pawn)
{
	// Shoot Motion Set
}

void ShootState::Exit(Pawn* pawn)
{
}

void ShootState::Update(Pawn* pawn)
{
	

	if (shootcooltime <0.00000001f) {
		g_sound->PlayShortSound(Issac::GetApp()->_allSounds.find("attack")->second, false, 0.3);
		// Bullet 생성
		Bullet* bullet = new Bullet;
		bullet->m_Physics->SetPosition(XMLoadFloat3(&pawn->GetPhysics().m_Position));
		bullet->m_Physics->SetVolume({ 10.f, 10.f, 10.f });
		bullet->m_Physics->SetMass(70.f);
		bullet->m_Physics->SetFriction(40.f);
		// Dir방향으로 shoot

		switch (pawn->GetPhysics().m_Direction)
		{
		case VK_UP:
			bullet->m_Physics->SetVelocity({ 0,speed,0 });
			break;
		case VK_DOWN:
			bullet->m_Physics->SetVelocity({ 0,-speed,0 });
			break;
		case VK_LEFT:
			bullet->m_Physics->SetVelocity({ -speed,0,0 });
			break;
		case VK_RIGHT:
			bullet->m_Physics->SetVelocity({ speed,0,0 });
			break;

		default:
			break;
		}

		bullet->m_Graphics->SetTexture("bullet");
		bullet->m_Graphics->SetTotal(1, 1);
		bullet->m_Graphics->SetCurrent(0, 0);

		Issac::GetApp()->m_Bullets.emplace_back(bullet);
		shootcooltime = 0.2;
	}

}

void DieState::Enter(Pawn* pawn)
{
	pawn->GetGraphics().SetTexture("die");
	pawn->GetGraphics().SetTotal(3, 8);
	pawn->GetGraphics().SetCurrent(1, m_VK);
}

void DieState::Exit(Pawn* pawn)
{

}

void DieState::Update(Pawn* pawn)
{

}
