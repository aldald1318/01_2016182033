#include "stdafx.h"
#include "GameplayScene.h"
#include "Issac.h"
#include "Controller.h"
#include "Command.h"
#include "Input.h"
#include "State.h"

#include "Map.h"
#include "Dumb.h"
#include "Character.h"

#include "Collision.h"

#include "TitleScene.h"
#include "SceneManager.h"

GameplayScene::GameplayScene()
{
}

GameplayScene::~GameplayScene()
{
	g_sound->StopBGSound(Issac::GetApp()->_allSounds.find("gameplay")->second);
	g_sound->StopBGSound(Issac::GetApp()->_allSounds.find("event")->second);
}

bool GameplayScene::Enter()
{
	LoadSounds();
	LoadResources();
	CreateCommands();
	CreateCharacter();

	Issac::GetApp()->m_Map = new Map;
	Issac::GetApp()->m_Map->SetPosition({ 0, 0, 0 });
	Issac::GetApp()->m_Map->SetTexture("map");

	m_Thor = Issac::GetApp()->AddObject<Dumb>();
	m_Thor->dieLayer = 1;
	m_Thor->GetPhysics().SetPosition({ -600.f,0.f,0.f });
	m_Thor->GetPhysics().SetVolume({ 50.f,50.f,50.f });
	m_Thor->GetPhysics().SetMass(70.f);
	m_Thor->GetPhysics().SetFriction(40.f);

	m_Thor->GetGraphics().SetPosition({ 0.f,0.f,0.f });
	m_Thor->GetGraphics().SetTexture("thor");
	m_Thor->GetGraphics().SetTotal(3, 4);
	m_Thor->GetGraphics().SetCurrent(1, 0);
	m_Thor->GetGraphics().HPHeight = {2,30,0};
	m_Thor->m_speed = 75;

	m_Captine = Issac::GetApp()->AddObject<Dumb>();
	m_Captine->dieLayer = 5;
	m_Captine->GetPhysics().SetPosition({ 700.f,10.f,0.f });
	m_Captine->GetPhysics().SetVolume({ 50.f,50.f,50.f });
	m_Captine->GetPhysics().SetMass(70.f);
	m_Captine->GetPhysics().SetFriction(40.f);

	m_Captine->GetGraphics().SetPosition({ 0.f,0.f,0.f });
	m_Captine->GetGraphics().SetTexture("captine");
	m_Captine->GetGraphics().SetTotal(3, 4);
	m_Captine->GetGraphics().SetCurrent(1, 0);
	m_Captine->GetGraphics().HPHeight = { 2,30,0 };
	m_Captine->m_speed = 70;

	///////////////////////////////////////////////////////////////

	m_Thor2 = Issac::GetApp()->AddObject<Dumb>();
	m_Thor2->dieLayer = 1;
	m_Thor2->GetPhysics().SetPosition({ -1000.f,0.f,0.f });
	m_Thor2->GetPhysics().SetVolume({ 50.f,50.f,50.f });
	m_Thor2->GetPhysics().SetMass(70.f);
	m_Thor2->GetPhysics().SetFriction(40.f);

	m_Thor2->GetGraphics().SetPosition({ 0.f,0.f,0.f });
	m_Thor2->GetGraphics().SetTexture("thor");
	m_Thor2->GetGraphics().SetTotal(3, 4);
	m_Thor2->GetGraphics().SetCurrent(1, 0);
	m_Thor2->GetGraphics().HPHeight = { 2,30,0 };
	m_Thor2->m_speed = 90;

	m_Captine2 = Issac::GetApp()->AddObject<Dumb>();
	m_Captine2->dieLayer = 5;
	m_Captine2->GetPhysics().SetPosition({ 1000.f,10.f,0.f });
	m_Captine2->GetPhysics().SetVolume({ 50.f,50.f,50.f });
	m_Captine2->GetPhysics().SetMass(70.f);
	m_Captine2->GetPhysics().SetFriction(40.f);

	m_Captine2->GetGraphics().SetPosition({ 0.f,0.f,0.f });
	m_Captine2->GetGraphics().SetTexture("captine");
	m_Captine2->GetGraphics().SetTotal(3, 4);
	m_Captine2->GetGraphics().SetCurrent(1, 0);
	m_Captine2->GetGraphics().HPHeight = { 2,30,0 };
	m_Captine2->m_speed = 85;

	///////////////////////////////////////////////////////////////

	m_Thor3 = Issac::GetApp()->AddObject<Dumb>();
	m_Thor3->dieLayer = 1;
	m_Thor3->GetPhysics().SetPosition({ -1500.f,0.f,0.f });
	m_Thor3->GetPhysics().SetVolume({ 50.f,50.f,50.f });
	m_Thor3->GetPhysics().SetMass(70.f);
	m_Thor3->GetPhysics().SetFriction(40.f);

	m_Thor3->GetGraphics().SetPosition({ 0.f,0.f,0.f });
	m_Thor3->GetGraphics().SetTexture("thor");
	m_Thor3->GetGraphics().SetTotal(3, 4);
	m_Thor3->GetGraphics().SetCurrent(1, 0);
	m_Thor3->GetGraphics().HPHeight = { 2,30,0 };
	m_Thor3->m_speed = 100;

	m_Captine3 = Issac::GetApp()->AddObject<Dumb>();
	m_Captine3->dieLayer = 5;
	m_Captine3->GetPhysics().SetPosition({ 1500.f,10.f,0.f });
	m_Captine3->GetPhysics().SetVolume({ 50.f,50.f,50.f });
	m_Captine3->GetPhysics().SetMass(70.f);
	m_Captine3->GetPhysics().SetFriction(40.f);

	m_Captine3->GetGraphics().SetPosition({ 0.f,0.f,0.f });
	m_Captine3->GetGraphics().SetTexture("captine");
	m_Captine3->GetGraphics().SetTotal(3, 4);
	m_Captine3->GetGraphics().SetCurrent(1, 0);
	m_Captine3->GetGraphics().HPHeight = { 2,30,0 };
	m_Captine3->m_speed = 95;

	return false;
}

void GameplayScene::Exit()
{
	Issac::GetApp()->ClearAll();
	delete Issac::GetApp()->m_Map;
	Issac::GetApp()->m_Map = nullptr;
}

void GameplayScene::Update(const float& fDeltaTime)
{
	Issac::GetApp()->m_Map->SetPosition(m_Player->GetPhysics().m_Position);

	// Input Update
	m_Player->Update(fDeltaTime);

	m_Captine->FollowCharacter(m_Player,fDeltaTime);
	m_Thor->FollowCharacter(m_Player,fDeltaTime);
	m_Captine2->FollowCharacter(m_Player, fDeltaTime);
	m_Thor2->FollowCharacter(m_Player, fDeltaTime);
	m_Captine3->FollowCharacter(m_Player, fDeltaTime);
	m_Thor3->FollowCharacter(m_Player, fDeltaTime);


	if(EventNum == -1)
		m_Hulk->FollowCharacter(m_Player, fDeltaTime);

	// Collision
	for (int i = 0; i < Issac::GetApp()->_physicsComponents.size(); ++i) {
		for (int j = i + 1; j < Issac::GetApp()->_physicsComponents.size(); ++j) {
			if (Collision::IsOverlap(static_cast<Actor*>(Issac::GetApp()->_allObjects[i]), static_cast<Actor*>(Issac::GetApp()->_allObjects[j]))) {
				if (static_cast<Actor*>(Issac::GetApp()->_allObjects[i])->GetGraphics().HPpercent <= 0){
					// ChangeState
					m_Player->ChangeState<DieState>(true, m_Player->dieLayer);
					m_Player->m_Controller = nullptr;
					g_sound->PlayShortSound(Issac::GetApp()->_allSounds.find("die")->second, false, 1);

					// ChangeScene
					Issac::GetApp()->ClearAll();
					SceneManager::GetApp()->ChangeScenes<TitleScene>(true);
					break;
				}
				else
					static_cast<Actor*>(Issac::GetApp()->_allObjects[i])->GetGraphics().HPpercent -= 30;
				Collision::ProcessCollision(static_cast<Actor*>(Issac::GetApp()->_allObjects[i]), static_cast<Actor*>(Issac::GetApp()->_allObjects[j]));
			}
		}
	}

	for (auto& b : Issac::GetApp()->m_Bullets)
	{
		for (auto& p : Issac::GetApp()->_allObjects)
		{
			if (p != nullptr) {
				if (Collision::IsOverlap(b, static_cast<Actor*>(p))) {
					Collision::ProcessCollision(b, static_cast<Actor*>(p));
					if (static_cast<Actor*>(p)->GetGraphics().HPpercent <= 0) {
						if (static_cast<Pawn*>(p)->isHulk)
						{
							g_sound->StopBGSound(Issac::GetApp()->_allSounds.find("gameplay")->second);
							g_sound->PlayBGSound(Issac::GetApp()->_allSounds.find("event")->second, false, 1);
							if (static_cast<Pawn*>(p)->hulklife > 1) {
								static_cast<Pawn*>(p)->hulklife -= 1;
								static_cast<Pawn*>(p)->dieLayer = 7;
								static_cast<Pawn*>(p)->GetPhysics().SetPosition({ m_Player->GetPhysics().m_Position.x + 100, m_Player->GetPhysics().m_Position.y + 100, 1500 });
								static_cast<Pawn*>(p)->GetPhysics().SetVolume({ 270.f,270.f,270.f });
								static_cast<Pawn*>(p)->GetPhysics().SetMass(70.f);
								static_cast<Pawn*>(p)->GetPhysics().SetFriction(40.f);

								static_cast<Pawn*>(p)->GetGraphics().SetTexture("hulk_angry");
								static_cast<Pawn*>(p)->GetGraphics().SetTotal(3, 4);
								static_cast<Pawn*>(p)->GetGraphics().SetCurrent(1, 0);
								static_cast<Pawn*>(p)->GetGraphics().HPHeight = { 2,130,0 };
								static_cast<Pawn*>(p)->GetGraphics().HPpercent = { 500 };
								static_cast<Pawn*>(p)->GetGraphics().HPSize = { 250,10 };
								static_cast<Dumb*>(p)->m_speed = 130;
							}
							else
							{
								static_cast<Pawn*>(p)->ChangeState<DieState>(true, static_cast<Pawn*>(p)->dieLayer);
								Issac::GetApp()->_allObjects[p->obj_index]->Release();
								Issac::GetApp()->_allObjects[p->obj_index] = NULL;

								EventWinner = true;
							}
						}
						else {
							static_cast<Pawn*>(p)->ChangeState<DieState>(true, static_cast<Pawn*>(p)->dieLayer);
							Issac::GetApp()->_allObjects[p->obj_index]->Release();
							Issac::GetApp()->_allObjects[p->obj_index] = NULL;
						}

						if(EventNum >2)
							EventNum -= 1;
						cout << EventNum << endl;
					}
					else
						static_cast<Actor*>(p)->GetGraphics().HPpercent -= 30;
				}
			}
		}
	}

	// Event (Boss 출현)
	// 보스잡으면 클리어
	if (EventNum == 6)
	{
		cout << "event on" << endl;
		m_Hulk = Issac::GetApp()->AddObject<Dumb>();
		m_Hulk->dieLayer = 6;
		m_Hulk->isHulk = true;
		m_Hulk->GetPhysics().SetPosition({ m_Player->GetPhysics().m_Position.x +100, m_Player->GetPhysics().m_Position.y + 100, 1500});
		m_Hulk->GetPhysics().SetVolume({ 80.f,80.f,80.f });
		m_Hulk->GetPhysics().SetMass(70.f);
		m_Hulk->GetPhysics().SetFriction(40.f);

		m_Hulk->GetGraphics().SetPosition({ 0.f,0.f,0.f });
		m_Hulk->GetGraphics().SetTexture("hulk");
		m_Hulk->GetGraphics().SetTotal(3, 4);
		m_Hulk->GetGraphics().SetCurrent(1, 0);
		m_Hulk->GetGraphics().HPHeight = { 2,45,0 };
		m_Hulk->GetGraphics().HPpercent = {100};
		m_Hulk->GetGraphics().HPSize = { 65,10 };
		m_Hulk->m_speed = 190;

		EventNum = -1;
		// 죽었을때 변신
	}

	//EventWinner = true;

	// ChangeScene
	if (EventWinner) {
		gamefinishtime += fDeltaTime;
		if (isKKK == false) {
			g_sound->StopBGSound(Issac::GetApp()->_allSounds.find("event")->second);
			g_sound->PlayBGSound(Issac::GetApp()->_allSounds.find("gameplay")->second, false, 1);
			gameover = Issac::GetApp()->AddObject<Dumb>();
			gameover->GetPhysics().SetPosition({ m_Player->GetPhysics().m_Position.x, m_Player->GetPhysics().m_Position.y-200,m_Player->GetPhysics().m_Position.z });
			gameover->GetPhysics().SetVolume({ 500,300,1 });

			gameover->GetGraphics().SetTexture("game_clear");
			gameover->GetGraphics().SetTotal(1, 1);
			gameover->GetGraphics().SetCurrent(0, 0);

			isKKK = true;
		}

		if (gamefinishtime > 5.0f)
		{
			nextScene1 = true;
		}

		if (nextScene1 == true) {
			Issac::GetApp()->ClearAll();
			SceneManager::GetApp()->ChangeScenes<TitleScene>(true);
		}
	}
}

void GameplayScene::CreateCharacter()
{
	m_Player = Issac::GetApp()->AddObject<Character>();
	m_Player->dieLayer = 3;
	m_Player->GetPhysics().SetPosition({ 50.f, 50.f, 0.f });
	m_Player->GetPhysics().SetVolume({ 50.f,50.f,50.f });
	m_Player->GetPhysics().SetMass(70.f);
	m_Player->GetPhysics().SetFriction(40.f);

	m_Player->GetGraphics().SetPosition({ 0.f,0.f,0.f });
	m_Player->GetGraphics().SetTexture("ironman");
	m_Player->GetGraphics().SetTotal(3,4);
	m_Player->GetGraphics().SetCurrent(1,0);
	m_Player->GetGraphics().HPHeight = { 2,30,0 };
	m_Player->GetGraphics().HPpercent = 3000;

	m_Player->SetController();

}

void GameplayScene::CreateCommands()
{
	const int WALK_SPEED = 40000.f;
	Input::CreateCommand<ForceCommand>(VK_UP, 0.f,WALK_SPEED,0.f);
	Input::CreateCommand<ForceCommand>(VK_DOWN, 0.f,-WALK_SPEED,0.f );
	Input::CreateCommand<ForceCommand>(VK_LEFT, -WALK_SPEED,0.f,0.f );
	Input::CreateCommand<ForceCommand>(VK_RIGHT, WALK_SPEED,0.f,0.f );

	Input::CreateCommand<StateCommand>(VK_SPACE);
	Input::CreateCommand<StateCommand>(VK_CONTROL);

}

void GameplayScene::LoadSounds()
{
	g_sound->StopBGSound(Issac::GetApp()->_allSounds.find("title")->second);
	g_sound->PlayBGSound(Issac::GetApp()->_allSounds.find("gameplay")->second, false, 1);
}

void GameplayScene::LoadResources()
{
	Issac::GetApp()->AddTexture("captine", "./Resources/Characters/captine.png");
	Issac::GetApp()->AddTexture("ironman", "./Resources/Characters/ironman.png");
	Issac::GetApp()->AddTexture("thor", "./Resources/Characters/thor.png");
	Issac::GetApp()->AddTexture("captine_marvel", "./Resources/Characters/captine_marvel.png");
	Issac::GetApp()->AddTexture("odin", "./Resources/Characters/odin.png");
	Issac::GetApp()->AddTexture("officer", "./Resources/Characters/officer.png");
	Issac::GetApp()->AddTexture("bullet", "./Resources/tear.png");
	Issac::GetApp()->AddTexture("map", "./Resources/map3.png");
	Issac::GetApp()->AddTexture("die", "./Resources/Characters/die.png");

	Issac::GetApp()->AddTexture("hulk_angry", "./Resources/Characters/hulk.png");
	Issac::GetApp()->AddTexture("hulk", "./Resources/Characters/hulk_human.png");

	Issac::GetApp()->AddTexture("game_clear", "./Resources/gameclear.png");
	Issac::GetApp()->AddTexture("game_over", "./Resources/gameclear.png");
}
