#pragma once
#include "IComponent.h"

#define GRAVITY -9.81f

class Actor;
class PhysicsComponent : public IComponent
{
public:
	explicit PhysicsComponent();
	virtual ~PhysicsComponent();

public:
	virtual void update(const float& deltaTime);
	
public:
	void ApplyForce(DirectX::XMVECTOR force);

public:
	void SetPosition(DirectX::XMVECTOR pos);
	void SetVolume(DirectX::XMFLOAT3 vol);

	void SetVelocity(DirectX::XMFLOAT3 vel);
	void SetAcceleration(DirectX::XMFLOAT3 acc);
	void SetMass(float mass);
	void SetFriction(float friction);

	void SetDirection(int dir);

public:
	DirectX::XMFLOAT3 m_PrevPosition;
	DirectX::XMFLOAT3 m_Position;
	DirectX::XMFLOAT3 m_Velocity;
	DirectX::XMFLOAT3 m_Acc;
	DirectX::XMFLOAT3 m_Volume;

	float	m_Mass;
	float	m_Friction;
	float	m_GravityMultipler;

	int		m_Direction;

public:
	int obj_index;
};

